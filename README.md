# 1.04 Primzahlenspiel

## Teil 1: Primzahlenspiel

**Voraussetzung**:
Vorlesung zu ausgewählten Sprachkonstrukten der Sprache Java.

**Ziel**:
Anwendung von `switch-case`-Konstrukten, Aufzählungstypen (`enum`) sowie Ausnahmebehandlung.

**Dauer**:
< 2 Stunden

## Aufgabenstellung
Schreiben Sie ein Java-Programm, welches ein Spiel realisiert, in dem ein Spieler zufällige Zahlen präsentiert bekommt und jeweils durch eine Eingabe entscheiden muss, ob diese Zahl eine Primzahl ist oder nicht.

### Schwierigkeitsstufen
Die zufälligen Zahlen werden unter Berücksichtigung einer Schwierigkeitsstufe generiert. Die Schwierigkeitsstufen sollen durch einen Aufzählungstyp (`enum Level`) festgelegt werden:

| Konstante    | Bedeutung                             |
|--------------|---------------------------------------|
| SIMPLE       | Generiert Zahlen zwischen 1 und 99    |
| INTERMEDIATE | Generiert Zahlen zwischen 100 und 999 |
| ADVANCED     | Generiert Zahlen zwischen 1000 und 9999 |

Zur Generierung einer Zufallszahl können Sie die Klasse `java.util.Random` und ihre Methode `nextInt` aus der Java-API verwenden:
[Java-API-Dokumentation für `java.util.Random`](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/Random.html)

### Klasse `PrimeGame`
Programmieren Sie zusätzlich die Klasse `PrimeGame` mit den folgenden Attributen und Methoden:

#### Attribute
| Attribut       | Repräsentation im Code | Bemerkung                                                                                  |
|----------------|------------------------|--------------------------------------------------------------------------------------------|
| Schwierigkeitsstufe des Spiels | `Level currentLevel`      | `Level` soll als Aufzählungstyp realisiert werden. Die Variable speichert die aktuelle Spielstufe. |
| Zahl, die im Spiel aktuell beurteilt werden soll | `int currentNumber`      | Das Spiel generiert eine zufällige Zahl (in Abhängigkeit von der Schwierigkeitsstufe) und speichert den Wert in `currentNumber`. |

Objekte der Klasse können durch den Default-Konstruktor erzeugt werden, der `currentLevel` auf `SIMPLE` setzt.

#### Methoden
| Methode                                   | Signatur                            | Bemerkung                                                                                                   |
|-------------------------------------------|-------------------------------------|-------------------------------------------------------------------------------------------------------------|
| Überprüfung, ob eine Zahl eine Primzahl ist | `public static boolean isPrime(int answer)` | Gibt `true` zurück, wenn `answer` eine Primzahl ist, sonst `false`.                                           |
| Erzeugt die nächste zu beurteilende Zahl  | `public void nextCurrentNumber()`   | Erzeugt eine neue Zufallszahl in Abhängigkeit vom aktuellen Schwierigkeitsgrad und speichert sie in `currentNumber`. |
| Erhöht die aktuelle Schwierigkeitsstufe des Spiels | `public void nextCurrentLevel()`    | Erhöht die Schwierigkeitsstufe des Spiels um eine Stufe durch Setzen des neuen Wertes in `currentLevel`. Tipp: Die Logik für den Wechsel des Levels muss möglichst in das `enum Level` ausgelagert werden! |

### Spiellogik in der `main()`-Methode
Das eigentliche Spiel soll in der `main`-Methode der Klasse `App` Ihres Gradle-Projekts programmiert werden. Das Spiel findet auf der Konsole statt. Die folgende Spiellogik soll umgesetzt werden:

1. Jeder Spieler muss 15 Zahlen dahingehend beurteilen, ob sie Primzahlen sind oder nicht.
2. Die Schwierigkeitsstufe steigt nach jeweils 5 Zahlen. Der Wechsel der Schwierigkeitsstufe geschieht automatisch und soll auf der Konsole ausgegeben werden, z.B. `You reached the next Level: INTERMEDIATE`.
3. Nach dem Start der Klasse `App` wird dem Spieler die erste zu beurteilende Zahl angezeigt, z.B. `Is 6 a prime number? (yes/no)`.
4. Mit Hilfe der Klasse `java.util.Scanner` kann eine Eingabe des Spielers realisiert werden.
5. Nach der Eingabe erhält der Spieler direkt die Antwort, ob er richtig lag, z.B. durch Ausgabe von `Correct` bzw. `Incorrect`.
6. Am Ende des Spiels, also nach 15 Zahlen, soll noch eine Statistik ausgegeben werden, z.B. `You made 6 out of 15!`.